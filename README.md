# BanzJS
## Banz is currently an Alpha SPA framework, however it really isn't.
(Don't tell anyone, but it's just a bit of jQuery)

Currently you have to link jQuery in every page you have (fixes on the way)

Just add app.js to your project and link it in all of your HTML pages.

Make sure you links id attributes are the name of the HTML page, minus the html extension

Example: this will link to about.html `<a id="about" href="#">About</a>`

[Check out the demo](https://banz.netlify.app/)